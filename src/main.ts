import Vue from 'vue'
import {BootstrapVue,BootstrapVueIcons } from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
import store from './store'
import PerfectScrollbar from "vue2-perfect-scrollbar";
import "vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css";
import VueSidebarMenu from 'vue-sidebar-menu'
import i18n from "@/lang/lang";

Vue.use(VueSidebarMenu)
Vue.use(PerfectScrollbar);
Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(PerfectScrollbar)
new Vue({
  router,
  vuetify,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
