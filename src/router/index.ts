import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import Stepone from '../components/Stepone.vue';
import Steptwo from '../components/Steptwo.vue';
import Stepthree from '../components/Stepthree.vue';
import Stepfour from '../components/Stepfour.vue';
// import Steponeparttwo from '../components/Steponeparttwo.vue';
import Stepfive from '../components/Stepfive.vue';
import Dashboardone from '../components/Dashboardone.vue';
import Radialprogressbar from '../components/Radialprogressbar.vue';
import IdeationMap from '../components/IdeationMap.vue';
import publication from '../components/publication.vue';

import Viewresults from '../components/Viewresults.vue';
Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Stepone',
    component: Stepone
  },
  {
    path: '/Steptwo',
    name: 'Steptwo',
    component: Steptwo
  },
  {
    path: '/Stepthree',
    name: 'Stepthree',
    component: Stepthree,
    // props: { Name: "this.kfs" },
  },

  {
    path: '/publication',
    name: 'publication',
    component: publication
    },
  {
    path: '/Stepfour',
    name: 'Stepfour',
    component: Stepfour
  },
  {
    path: '/Stepfive',
    name: 'Stepfive',
    component: Stepfive
  },
  {
    path: '/Dashboardone',
    name: 'Dashboardone',
    component: Dashboardone
  },
  {
    path: '/Radialprogressbar',
    name: 'Radialprogressbar',
    component: Radialprogressbar
  },
  {
    path: '/Viewresults',
    name: 'Viewresults',
    component: Viewresults
  },
  {
    path: '/IdeationMap',
    name: 'IdeationMap',
    component: IdeationMap
  },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
