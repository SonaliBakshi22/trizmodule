import Vue from "vue";
import VueI18n from "vue-i18n";
import axios from 'axios'

import us from './i18n/us.json';

Vue.use(VueI18n);

const defaultLocale = "us";

const locales: { [key: string]: any } = {
  us: us,
};

const i18n = new VueI18n({
  locale: defaultLocale,
  messages: locales
});

export const loadedLanguages = ['us'] // our default language that is preloaded

export const setI18nLanguage = (lang: string) => {
  i18n.locale = lang
  axios.defaults.headers.common['Accept-Language'] = lang
  document.querySelector('html')!.setAttribute('lang', lang)
  return lang
}

export const loadLanguageAsync = async (lang = defaultLocale) => {
  // If the same language
  if (i18n.locale === lang) {
    return setI18nLanguage(lang)
  }

  // If the language was already loaded
  if (loadedLanguages.includes(lang)) {
    return setI18nLanguage(lang)
  }
  // If the language hasn't been loaded yet
  const messages = await import(`./i18n/${lang ?? 'us'}.json`)

  i18n.setLocaleMessage(lang, messages.default)
  loadedLanguages.push(lang)
  return setI18nLanguage(lang)
}

// "(\w|\s)+": "

export default i18n;
